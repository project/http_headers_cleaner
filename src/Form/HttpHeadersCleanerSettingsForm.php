<?php

namespace Drupal\http_headers_cleaner\Form;

use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\http_headers_cleaner\Service\HttpHeadersCleanerSettings;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Settings form for Http-Header Cleaner module.
 *
 * @package Drupal\http_headers_cleaner\Form
 */
class HttpHeadersCleanerSettingsForm extends FormBase implements TrustedCallbackInterface {

  /**
   * Settings.
   *
   * @var \Drupal\http_headers_cleaner\Service\HttpHeadersCleanerSettings
   */
  protected $settings;

  /**
   * HttpHeadersCleanerSettingsForm constructor.
   *
   * @param \Drupal\http_headers_cleaner\Service\HttpHeadersCleanerSettings $settings
   *   The config.
   */
  public function __construct(HttpHeadersCleanerSettings $settings) {
    $this->settings = $settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get(HttpHeadersCleanerSettings::SERVICE_NAME)
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'http_headers_cleaner.settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $this->initHeadersForm($form, $form_state);
    $this->initMeta($form, $form_state);

    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => $this->t('Save'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $types = [
      HttpHeadersCleanerSettings::TYPE_HEADERS,
      HttpHeadersCleanerSettings::TYPE_METATAGS,
    ];

    foreach ($types as $type) {
      $this->settings
        ->setEnabled($type, (bool) $form_state->getValue($type)[HttpHeadersCleanerSettings::FIELD_ENABLED])
        ->setConfig($type, $this->getConfigFromValue($form_state->getValue($type)[HttpHeadersCleanerSettings::FIELD_CONFIG]));
    }

    $this->settings
      ->save()
      ->clearCache();
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return [
      'validateConfiguration',
    ];
  }

  /**
   * Validate configuration.
   *
   * @param array $element
   *   The element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The formstate.
   * @param array $form
   *   The form.
   */
  public function validateConfiguration(array $element, FormStateInterface $form_state, array &$form) {
    try {
      $this->getConfigFromValue($element['#value']);
    }
    catch (\Exception $e) {
      $form_state->setError($element, $e->getMessage());
    }
  }

  /**
   * Return the formatted configuration.
   *
   * @param string $value
   *   The value.
   *
   * @return array
   *   The configuration value.
   *
   * @throws \Exception
   */
  public function getConfigFromValue($value) {
    if (!empty($value)) {
      $value = Yaml::decode($value);
      $this->settings->validateConfiguration($value);
    }
    else {
      $value = [];
    }

    return $value;
  }

  /**
   * Init headers form part.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The formstate.
   */
  protected function initHeadersForm(array &$form, FormStateInterface $form_state) {
    $type = HttpHeadersCleanerSettings::TYPE_HEADERS;
    $form[$type] = [
      '#type'  => 'details',
      '#title' => $this->t('HTTP-Headers'),
      '#open'  => TRUE,
      '#tree'  => TRUE,
    ];

    $default_value = $form_state->get($type)[HttpHeadersCleanerSettings::FIELD_ENABLED] ?? $this->settings->isEnabled($type);
    $form[$type][HttpHeadersCleanerSettings::FIELD_ENABLED] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enabled'),
      '#default_value' => $default_value,
    ];

    $default_value = $form_state->get($type)[HttpHeadersCleanerSettings::FIELD_CONFIG] ?? Yaml::encode($this->settings->getConfig($type));
    $form[$type][HttpHeadersCleanerSettings::FIELD_CONFIG] = [
      '#type'             => 'textarea',
      '#title'            => $this->t('Elements to remove'),
      '#default_value'    => $default_value,
      '#element_validate' => [[$this, 'validateConfiguration']],
      '#description'      => $this->t('Define an associative array {header-id: [pattern1, pattern2]} <br/> If no pattern is defined, the header will be removed.<br/> If patterns are defined, only matching elements will be removed.<br/><br/>Ex.') . $this->getHeaderExample(),
      '#attributes'       => ['data-yaml-editor' => 'true'],
    ];
  }

  /**
   * Return the Headers conf example.
   *
   * @return string
   *   The example.
   */
  protected function getHeaderExample() {
    $data = Yaml::encode(
      [
        "x-drupal-dynamic-cache"  => NULL,
        "x-drupal-cache-tags"     => NULL,
        "x-drupal-cache-contexts" => NULL,
        "x-drupal-cache-max-age"  => NULL,
        "x-generator"             => NULL,
        "x-drupal-cache"          => NULL,
        "link"                    => [
          '/rel="revision"/',
          '/rel="shortlink"/',
          '/rel="delete-form"/',
          '/rel="delete-multiple-form"/',
          '/rel="edit-form"/',
          '/rel="version-history"/',
          '/rel="revision"/',
          '/rel="drupal:content-translation-overview"/',
          '/rel="drupal:content-translation-add"/',
          '/rel="drupal:content-translation-edit"/',
          '/rel="drupal:content-translation-delete"/',
        ],
      ]
    );

    return '<pre>' . $data . '</pre>';
  }

  /**
   * Init metatag form part.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The formstate.
   */
  protected function initMeta(array &$form, FormStateInterface $formState) {
    $type = HttpHeadersCleanerSettings::TYPE_METATAGS;
    $form[$type] = [
      '#type'  => 'details',
      '#title' => $this->t('Meta tags'),
      '#open'  => TRUE,
      '#tree'  => TRUE,
    ];

    $default_value = $formState->get($type)[HttpHeadersCleanerSettings::FIELD_ENABLED] ?? $this->settings->isEnabled($type);
    $form[$type][HttpHeadersCleanerSettings::FIELD_ENABLED] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enabled'),
      '#default_value' => $default_value,
    ];

    $default_value = $formState->get($type)[HttpHeadersCleanerSettings::FIELD_CONFIG] ?? Yaml::encode($this->settings->getConfig($type));
    $form[$type][HttpHeadersCleanerSettings::FIELD_CONFIG] = [
      '#type'             => 'textarea',
      '#title'            => $this->t('Elements to remove'),
      '#default_value'    => $default_value,
      '#element_validate' => [[$this, 'validateConfiguration']],
      '#description'      => $this->t('Define an associative array {tag: [ {attribute : [pattern1, pattern2]}]} <br/><br/> Ex:') . $this->getMetaExample(),
      '#attributes'       => ['data-yaml-editor' => 'true'],
    ];
  }

  /**
   * Return the meta example.
   *
   * @return string
   *   The example.
   */
  protected function getMetaExample() {
    $data = Yaml::encode(
      [
        'meta' => [
          'name' => [
            '/Generator/',
          ],
        ],
        'link' => [
          'rel' => [
            '/delete-form/',
            '/shortlink/',
            '/revision/',
            '/delete-multiple-form/',
            '/edit-form/',
            '/version-history/',
            '/drupal:content-translation-overview/',
            '/drupal:content-translation-add/',
            '/drupal:content-translation-edit/',
            '/drupal:content-translation-delete/',
          ],
        ],
      ]
    );

    return '<pre>' . $data . '</pre>';
  }

}
