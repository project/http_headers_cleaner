<?php

namespace Drupal\http_headers_cleaner\Service;

use Drupal\Core\Config\Config;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Plugin\CachedDiscoveryClearerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Configuration tool.
 *
 * @package Drupal\http_headers_cleaner\Service
 */
class HttpHeadersCleanerSettings {

  use StringTranslationTrait;

  /**
   * Service Name.
   *
   * @const string
   */
  const SERVICE_NAME = 'http_headers_cleaner.settings';

  /**
   * Headers Configuration ID.
   *
   * @const string
   */
  const TYPE_HEADERS = 'headers';

  /**
   * Metatag Configuration ID.
   *
   * @const string
   */
  const TYPE_METATAGS = 'metatags';

  /**
   * Field enabled.
   *
   * @const string
   */
  const FIELD_ENABLED = 'enabled';

  /**
   * Field configuration.
   *
   * @const string
   */
  const FIELD_CONFIG = 'conf';

  /**
   * The immutable configuration.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * The editable configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected Config $editable;

  /**
   * Update.
   *
   * @var bool
   */
  protected ?bool $updateNeeded = FALSE;

  /**
   * The cache clearer.
   *
   * @var \Drupal\Core\Plugin\CachedDiscoveryClearerInterface
   */
  protected CachedDiscoveryClearerInterface $cacheDiscoveryClearer;

  /**
   * HttpHeadersCleanerSettings constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   * @param \Drupal\Core\Plugin\CachedDiscoveryClearerInterface $cached_discovery_clearer
   *   The cache clearer.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    CachedDiscoveryClearerInterface $cached_discovery_clearer,
  ) {
    $this->config = $config_factory->get(static::SERVICE_NAME);
    $this->editable = $config_factory->getEditable(static::SERVICE_NAME);
    $this->cacheDiscoveryClearer = $cached_discovery_clearer;
  }

  /**
   * Singleton.
   *
   * @return static
   *   The singleton.
   */
  public static function me() {
    return \Drupal::service(static::SERVICE_NAME);
  }

  /**
   * Return the value.
   *
   * @param string|null $key
   *   The key.
   *
   * @return array|mixed|null
   *   The value.
   */
  public function get(string $key = NULL) {
    $conf = $this->updateNeeded ? $this->editable : $this->config;
    if ($key) {
      return $conf->get($key) ?: [];
    }

    return $conf->getRawData() ?: [];
  }

  /**
   * Set the value.
   *
   * @param string $key
   *   The key.
   * @param mixed $value
   *   The value.
   *
   * @return $this
   */
  public function set($key, $value) {
    $this->updateNeeded = TRUE;
    $this->editable->set($key, $value);

    return $this;
  }

  /**
   * Save.
   *
   * @return $this
   */
  public function save() {
    if ($this->updateNeeded) {
      $this->editable->save();
    }

    return $this;
  }

  /**
   * Return configuration.
   *
   * @param string $type
   *   The type.
   *
   * @return array
   *   The config.
   */
  public function getConfig($type) {
    $config = [];
    $data = $this->get($type);
    if ($data && isset($data[static::FIELD_CONFIG])) {
      $config = $data[static::FIELD_CONFIG];
    }

    return $config;
  }

  /**
   * Check if process is enabled.
   *
   * @param string $type
   *   The type.
   *
   * @return bool
   *   The status.
   */
  public function isEnabled($type) {
    $enabled = FALSE;
    $data = $this->get($type);
    if ($data && isset($data[static::FIELD_ENABLED])) {
      $enabled = $data[static::FIELD_ENABLED];
    }

    return $enabled;
  }

  /**
   * Update enabled value.
   *
   * @param string $type
   *   The type.
   * @param bool $isEnabled
   *   The value.
   *
   * @return $this
   */
  public function setEnabled($type, $isEnabled) {
    $data = $this->get($type) ?: [];
    $data[static::FIELD_ENABLED] = $isEnabled;
    $this->set($type, $data);

    return $this;
  }

  /**
   * Update the config.
   *
   * @param string $type
   *   The type.
   * @param array $config
   *   The config.
   *
   * @return $this
   */
  public function setConfig($type, array $config) {
    $this->validateConfiguration($config);

    $data = $this->get($type) ?: [];
    $data[static::FIELD_CONFIG] = $config;
    $this->set($type, $data);

    return $this;
  }

  /**
   * Validate configuration.
   *
   * @param array $config
   *   The config.
   *
   * @throws \Exception
   */
  public function validateConfiguration(array $config) {
    if (!is_array($config)) {
      throw new \Exception('HTTP-Headers Cleaner config is not an array');
    }
  }

  /**
   * Clear the cache.
   *
   * @return $this
   */
  public function clearCache() {
    // Clear all plugin caches.
    $this->cacheDiscoveryClearer->clearCachedDefinitions();

    return $this;
  }

}
