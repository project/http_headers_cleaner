<?php

namespace Drupal\http_headers_cleaner\Service;

/**
 * Clean meta.
 *
 * Please see /admin/config/system/http-headers-cleaner.
 *
 * @package Drupal\http_headers_cleaner\Service
 */
class MetaCleaner {

  /**
   * Service name.
   *
   * @const string
   */
  const SERVICE_NAME = 'http_headers_cleaner.meta_cleaner';

  /**
   * Settings.
   *
   * @var \Drupal\http_headers_cleaner\Service\HttpHeadersCleanerSettings
   */
  protected HttpHeadersCleanerSettings $settings;

  /**
   * The list of configured patterns.
   *
   * @var array|mixed
   */
  protected $patterns;

  /**
   * MetaCleaner constructor.
   *
   * @param \Drupal\http_headers_cleaner\Service\HttpHeadersCleanerSettings $settings
   *   The settings.
   */
  public function __construct(HttpHeadersCleanerSettings $settings) {
    $this->settings = $settings;
    $this->patterns = $this->settings->getConfig(HttpHeadersCleanerSettings::TYPE_METATAGS);
  }

  /**
   * Retourne le singleton.
   *
   * @return static
   *   Le singleton.
   */
  public static function me() {
    return \Drupal::service(static::SERVICE_NAME);
  }

  /**
   * Alter meta attachments.
   *
   * @param array $attachments
   *   The attachments array.
   */
  public function alterAttachments(array &$attachments) {
    if ($this->settings->isEnabled(HttpHeadersCleanerSettings::TYPE_METATAGS)) {

      // Process html_head.
      if (isset($attachments['#attached']['html_head'])) {
        $this->processHtmlHead($attachments['#attached']['html_head']);
      }

      // Process html_head_link.
      if (isset($attachments['#attached']['html_head_link'])) {
        $this->processHtmlHeadLink($attachments['#attached']['html_head_link']);
      }
    }
  }

  /**
   * Clean Head.
   *
   * @param array $html_head
   *   The head tags value list.
   */
  protected function processHtmlHead(array &$html_head) {
    for ($key = count($html_head); $key--; $key > -1) {
      if ($type_patterns = $this->getTypePatternFromMeta($html_head[$key][0])) {
        if (isset($html_head[$key][0]['#attributes'])) {
          if ($this->matchesAttributes($html_head[$key][0]['#attributes'], $type_patterns)) {
            unset($html_head[$key]);
          }
        }
      }
    }
  }

  /**
   * Clean head links.
   *
   * @param array $html_head_link
   *   The head links list.
   */
  protected function processHtmlHeadLink(array &$html_head_link) {
    if (isset($this->patterns['link'])) {
      $type_patterns = $this->patterns['link'];
      if (!empty($type_patterns)) {
        for ($key = count($html_head_link); $key--; $key > -1) {
          if (isset($html_head_link[$key])) {
            foreach ($html_head_link[$key] as $data) {
              // Only attributes.
              if (is_array($data) && $this->matchesAttributes($data, $type_patterns)) {
                unset($html_head_link[$key]);
              }
            }
          }
        }
      }
    }
  }

  /**
   * Return true if meta matches with pattern.
   *
   * @param array $metadata
   *   The metat data.
   *
   * @return mixed
   *   The type.
   */
  protected function getTypePatternFromMeta(array $metadata) {
    if (isset($metadata['#tag']) && array_key_exists($metadata['#tag'], $this->patterns)) {
      return $this->patterns[$metadata['#tag']];
    }
    return NULL;
  }

  /**
   * Check if attributes matches type patterns.
   *
   * @param array $attributes
   *   Attributes.
   * @param string $type_patterns
   *   Type pattern.
   *
   * @return bool
   *   Match status.
   */
  protected function matchesAttributes(array $attributes, $type_patterns) {
    if (!empty($type_patterns)) {
      // Check attributes.
      foreach ($attributes as $key => $value) {
        // Check attributes key.
        if (array_key_exists($key, $type_patterns)) {
          foreach ($type_patterns[$key] as $pattern) {
            // Check attributes value.
            if (preg_match($pattern, $value)) {
              return TRUE;
            }
          }
        }
      }
    }

    return FALSE;
  }

}
