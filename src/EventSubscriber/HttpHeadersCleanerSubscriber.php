<?php

namespace Drupal\http_headers_cleaner\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\http_headers_cleaner\Service\HttpHeadersCleanerSettings;

/**
 * Subscriber that filters http headers.
 */
class HttpHeadersCleanerSubscriber implements EventSubscriberInterface {

  /**
   * Constructs a HttpHeadersCleanerSubscriber object.
   */
  public function __construct(
    protected readonly HttpHeadersCleanerSettings $settings,
  ) {
  }

  /**
   * Kernel response event handler.
   */
  public function onKernelResponse(ResponseEvent $event): void {
    if (
      $event->getRequestType() === HttpKernelInterface::MAIN_REQUEST
      && $this->settings->isEnabled(HttpHeadersCleanerSettings::TYPE_HEADERS)
    ) {
      $this->cleanHeaders($event->getResponse());
    }
  }

  /**
   * Clean headers.
   *
   * @param \Symfony\Component\HttpFoundation\Response $response
   *   The response.
   */
  protected function cleanHeaders(Response $response) {
    $headers = $response->headers;
    $headers_array = $headers->all();

    foreach ($this->settings->getConfig(HttpHeadersCleanerSettings::TYPE_HEADERS) as $key => $patternsList) {
      if (is_array($patternsList)) {
        if (isset($headers_array[$key]) && is_array($headers_array[$key])) {
          $clean = array_filter($headers_array[$key], function ($item) use ($patternsList) {
            return !$this->matchPatterns($item, $patternsList);
          });

          $headers->set($key, $clean);
        }
      }
      else {
        $headers->remove($key);
      }
    }

    return $response;
  }

  /**
   * Return true if matches any pattern.
   *
   * @param string $item
   *   The item.
   * @param array $patterns_list
   *   The patterns list.
   */
  protected function matchPatterns($item, array $patterns_list) {
    foreach ($patterns_list as $pattern) {
      $match = [];
      if (preg_match($pattern, $item, $match)) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::RESPONSE => ['onKernelResponse'],
    ];
  }

}
