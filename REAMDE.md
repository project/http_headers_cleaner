CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* Troubleshooting
* FAQ
* Maintainers


INTRODUCTION
------------
The Http Headers Cleaner remove useless or insecure information from http
headers or meta tags.
For example, it can remove the x-generator information.
The elements to remove are fully configurable and you can define several pattern
rules to identify which elements you to remove.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


RECOMMENDED MODULES
-------------------

- Yaml Editor (https://www.drupal.org/project/yaml_editor):
  Simplify yaml configuration.


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

* Configure the user permissions in Administration » People » Permissions:

  - Use the administration pages and help (System module)

* Access administration form in Administration » Configuration » System »
  HTTP-Headers cleaner settings.
  - Enable HTTP-headers and use the yaml editor :
     Define an associative array {header-id: [pattern1, pattern2]}
      If no pattern is defined, the header will be removed.
      If patterns are defined, only matching elements will be removed.
      Ex.
```
x-drupal-dynamic-cache: null
x-drupal-cache-tags: null
x-drupal-cache-contexts: null
x-drupal-cache-max-age: null
x-generator: null
x-drupal-cache: null
link:
- '/rel="revision"/'
- '/rel="shortlink"/'
- '/rel="delete-form"/'
- '/rel="delete-multiple-form"/'
- '/rel="edit-form"/'
- '/rel="version-history"/'
- '/rel="revision"/'
- '/rel="drupal:content-translation-overview"/'
- '/rel="drupal:content-translation-add"/'
- '/rel="drupal:content-translation-edit"/'
- '/rel="drupal:content-translation-delete"/'
```

  - Enable Meta tags and use the yaml editor :
    Define an associative array {tag: [ {attribute : [pattern1, pattern2]}]}
    Ex:
```

meta:
  name:
    - /Generator/
link:
  rel:
    - /delete-form/
    - /shortlink/
    - /revision/
    - /delete-multiple-form/
    - /edit-form/
    - /version-history/
    - '/drupal:content-translation-overview/'
    - '/drupal:content-translation-add/'
    - '/drupal:content-translation-edit/'
    - '/drupal:content-translation-delete/'
```

MAINTAINERS
-----------

Current maintainers:
* Thomas Sécher (tsecher) - https://www.drupal.org/u/tsecher


Supporting organization:
* Conserto - https://conserto.pro/
